import React from 'react'
import { Link } from 'react-router'
import './PageLayout.scss'
import { connect } from 'react-redux'
import { actions } from '../../../routes/Login/modules/counter'

const mapDispatchToProps = {
  ...actions
}
const mapStateToProps = (state) => ({
  isLogged: state.login.isLogged
})

export class PageLayout extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isLogged: false
    }
  }


  render () {
    this.props.checkLogin()
    this.state.isLogged = this.props.isLogged
    console.log('this.props.isLogged--', this.props.isLogged)
    // console.log('this.props.isLogged--', this.state.isLogged)
    return (
      <div className='text-center col-lg-12 container-fluid'>
        <h1>React Redux Starter Kit</h1>
        <Link to='/something' activeClassName='page-layout__nav-item--active'>News</Link>
        {this.state.isLogged === true && <div>
          <Link to='/profile' activeClassName='page-layout__nav-item--active'>Profile</Link>
          {' · '}
          <a href='/auth/logout'>Logout</a>
        </div>
        }
        {this.state.isLogged === false && (
          <div>
            <Link to='/login' activeClassName='page-layout__nav-item--active'>Login</Link>
            {' · '}
            <Link to='/register' activeClassName='page-layout__nav-item--active'>Register</Link>
          </div>
        )}
        <div className='page-layout__viewport col-lg-12'>
          {this.props.children}
        </div>

      </div>
    )
  }
}
// export default PageLayout

export default connect(mapStateToProps, mapDispatchToProps)(PageLayout)
