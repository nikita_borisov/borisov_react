import { combineReducers } from 'redux'
import locationReducer from './location'
import LoginReducer from '../routes/Login/modules/counter'
import ProfileReducer from '../routes/Profile/modules/counter'
import NewsReducer from '../routes/News/modules/news'
export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    location: locationReducer,
    login: LoginReducer,
    profile: ProfileReducer,
    something: NewsReducer,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
