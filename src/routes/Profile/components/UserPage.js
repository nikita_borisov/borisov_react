import React from 'react'
import _ from 'lodash'

export class LoginForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      surname: '',
      email: '',
      username: '',
      newsArray: []
    }
    this.handleChangeEmail = this.handleChangeEmail.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.handleSubmitForm = this.handleSubmitForm.bind(this)
    this.qqz = false
  }

  componentDidMount () {
    this.props.getUser()
    this.props.getNews()
    this.props.checkLogin('userpage')
  }

  handleChangeEmail (event) {
    this.setState({email: event.target.value})
  }

  handleChangePassword (event) {
    this.setState({password: event.target.value})
  }

  handleSubmitForm (event) {
    event.preventDefault()
    // let formData = Object.assign({}, this.state)
  }

  render () {
    this.state.name = this.props.name
    this.state.surname = this.props.surname
    this.state.email = this.props.email
    this.state.username = this.props.username
    this.state.newsArray = this.props.newsArr
    this.state.newsArray = _.filter(this.state.newsArray, (news) => {
      return news.username === this.state.username
    })
    return (
      <div>
        <h2>{this.state.name} {this.state.surname}</h2>
        <span>{this.state.email}</span>
        <br />
        <span>{this.state.username}</span>
        { this.state.newsArray.map((news, item) => {
          return (
            <div className='clearfix' key={item}>
              <h2 className='text-left'>{news.title}</h2>
              <p className='text-left'>{news.text}</p>
              <div className='pull-left'>
                {news.tags.map((tag, num) => {
                  return (
                    <span className='btn btn-sm btn-info' key={num}>{tag}</span>
                  )
                })}
                <div>{news.username}</div>
              </div>
            </div>
          )
        }) }
      </div>
    )
  }
}

export default LoginForm
