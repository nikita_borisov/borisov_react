  // ------------------------------------
// Constants
// ------------------------------------
import axios from 'axios'
import { browserHistory } from 'react-router'
export const GET_USER = 'GET_USER'
export const COUNTER_DOUBLE_ASYNC = 'COUNTER_DOUBLE_ASYNC'

// ------------------------------------
// Actions
// ------------------------------------

/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk! */

export function getUser () {
  return dispatch => {
    axios({
      url: '/auth/profile',
      method: 'get'
    }).then((response) => {
      if (response) {
        return dispatch({
          type: GET_USER,
          payload: response.data
        })
      }
    }).catch((err) => {
      console.log('err--', err)
    })

    // sendPost: .then(function (response) {
    //   console.log('response--', response)
    //
    //   console.log('response--', response)
    // }).catch(function (err) {
    //
    //   console.log('!!!error ', err)
    // })
  }
}

export const actions = {
  getUser
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_USER]    : (state, action) => {
    return Object.assign({}, state, {
      name: action.payload.name,
      surname: action.payload.surname,
      email: action.payload.email,
      username: action.payload.username
    })
  },
  [COUNTER_DOUBLE_ASYNC] : (state, action) => state * 2
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  name: '',
  surname: '',
  email: '',
  username: ''
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
