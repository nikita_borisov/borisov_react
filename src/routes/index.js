// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/PageLayout/components/PageLayout'
import Counter from './Register'
import Login from './Login'
import NewsRoute from './News'
import Profile from './Profile'

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = (store) => ({
  path        : '/',
  component   : CoreLayout,
  // indexRoute  : Register(store),
  childRoutes : [
    NewsRoute(store), Counter(store), Login(store), Profile(store)
  ]
})

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Register').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes
