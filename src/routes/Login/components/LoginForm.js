import React from 'react'

export class LoginForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      email: '',
      password: ''
    }
    this.handleChangeEmail = this.handleChangeEmail.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.handleSubmitForm = this.handleSubmitForm.bind(this)
    this.facebookLogin = this.facebookLogin.bind(this)
    this.qqz = false
  }

  handleChangeEmail (event) {
    this.setState({email: event.target.value})
  }

  handleChangePassword (event) {
    this.setState({password: event.target.value})
  }

  handleSubmitForm (event) {
    event.preventDefault()
    let qqz = Object.assign({}, this.state)
    // let formData = new FormData()
    // formData.append('title', this.state.title)
    // formData.append('text', this.state.text)
    // formData.append('tags', this.state.tags)
    // formData.append('image', this.fileInput.files[0])
    this.props.loginForm(qqz)
    this.state.name = ''
    this.state.surname = ''
    this.state.email = ''
    this.state.password = ''
    // console.log('formData--', formData)
  }
  facebookLogin () {
    this.props.loginFacebook()
  }
  render () {

    this.qqz = this.props.loginError
    return (
      <div>
        <form onSubmit={this.handleSubmitForm}>
          <label>
            Email*
            <input type='email' value={this.state.email} onChange={this.handleChangeEmail} className='form-control'/>
          </label>
          <br />
          <label>
            Password*
            <input type='password' value={this.state.password} onChange={this.handleChangePassword}
                   className='form-control'/>
          </label>
          {this.qqz && <div style={{color: 'red'}}>Incorrect Email or password</div>}

          <br />
          <button className='btn btn-lg'>Sign up</button>
        </form>
        {/*<button className='btn btn-info btn-lg' onClick={this.facebookLogin}>Continue with Facebook</button>*/}
        <a href='/auth/facebook'>FB</a>
      </div>
    )
  }
}

export default LoginForm
