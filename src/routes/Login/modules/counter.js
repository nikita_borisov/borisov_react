// ------------------------------------
// Constants
// ------------------------------------
import axios from 'axios'
import { browserHistory } from 'react-router'
export const GET_FORM = 'GET_FORM'
export const COUNTER_DOUBLE_ASYNC = 'COUNTER_DOUBLE_ASYNC'
export const CHECK_LOGIN = 'CHECK_LOGIN'
export const FACEBOOK_LOGIN = 'FACEBOOK_LOGIN'

// ------------------------------------
// Actions
// ------------------------------------

/*  This is a thunk, meaning it is a function that immediately
 returns a function for lazy evaluation. It is incredibly useful for
 creating async actions, especially when combined with redux-thunk! */

export function loginForm (value) {
  return dispatch => {
    axios({
      url: '/auth/login',
      method: 'post',
      data: value
    }).then((response) => {
      // console.log('response--', response)
      if (response.data) {
        browserHistory.push('/profile')
      }
      return dispatch({
        type: GET_FORM,
        payload: value,
        loginSuccess: false
      })
    }).catch((response) => {
      return dispatch({
        type: GET_FORM,
        payload: value,
        loginSuccess: true
      })
    })
  }
}

export function checkLogin (component) {
  return dispatch => {
    axios({
      url: '/auth/profile',
      method: 'get'
    }).then((response) => {
      // console.log('response.data--', response.data)
      if (typeof response.data == 'object') {
        return dispatch({
          type: CHECK_LOGIN,
          logStatus: true,
          payload: response.data
        })
      }
      else if (typeof response.data != 'object' && component) {
        browserHistory.push('/login')
      }
    }).catch((err) => {
      console.log('err--', err)
    })
  }
}

export function loginFacebook () {
  return dispatch => {
    axios({
      url: '/auth/facebook',
      method: 'get'
    }).then((response) => {
      // console.log('response--', response)
      return dispatch({
        type: FACEBOOK_LOGIN,
        payload: response.data
      })
    }).catch((err) => {
      console.log('err--', err)
    })
  }
}
// sendPost: .then(function (response) {
//   console.log('response--', response)
//
//   console.log('response--', response)
// }).catch(function (err) {
//
//   console.log('!!!error ', err)
// })

export const actions = {
  loginForm,
  checkLogin,
  loginFacebook
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_FORM]: (state, action) => {
    return Object.assign({}, state, {
      // name: action.payload,
      // surname: action.payload,
      email: action.payload,
      password: action.payload,
      isLogged: action.loginSuccess
    })
  },
  [COUNTER_DOUBLE_ASYNC]: (state, action) => state * 2,
  [CHECK_LOGIN]: (state, action) => {
    // console.log('action.logStatus--', action.logStatus)
    return Object.assign({}, state, {
      isLogged: action.logStatus
    })
  },
  [FACEBOOK_LOGIN]: (state, action) => {
    return Object.assign({}, state, {
      isLogged: action.logStatus
    })
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  email: '',
  password: '',
  logError: false,
  isLogged: false
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
