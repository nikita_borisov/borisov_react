import React from 'react'
import NameForm from './forms'
// import NewsLayout from './NewsLayout'

export const News = props => {
  return (
    <div className='col-lg-12'>
      <h2>News: </h2>
      {' '}
      <div>
        <NameForm
          getForm={props.getForm}
          getNews={props.getNews}
          pagination={props.pagination}
          countPages={props.countPages}
          MegaNews={props.mainNews}
        />
      </div>
    </div>
  )
}

export default News
