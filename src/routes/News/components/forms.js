import React from 'react'
import SearchInput, { createFilter } from 'react-search-input'
import _ from 'lodash'
import styles from './styles/StyleNews.scss'
import axios from 'axios'

// const KEYS_TO_FILTERS = ['title', 'text', 'tags']
const itemsOnPage = 3
const className = 'search-input'

export class NameForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      title: '',
      text: '',
      tags: '',
      activePage: 1,
      searchResults: [],
      searchTerm: '',
      searchTags: '',
      pageContent: [],
      searchTag: '',
      username: '',
      data: {}
    }
    this.handleChangeTitle = this.handleChangeTitle.bind(this)
    this.handleChangeText = this.handleChangeText.bind(this)
    this.handleChangeTags = this.handleChangeTags.bind(this)
    this.searchUpdated = this.searchUpdated.bind(this)
    this.handlePageClick = this.handlePageClick.bind(this)
    this.handleSubmitForm = this.handleSubmitForm.bind(this)
    this.searchByTag = this.searchByTag.bind(this)
    // this.handleSubmitImage = this.handleSubmitImage.bind(this)
  }

  componentDidMount () {
    // this.props.getNews(this.state.activePage)
    this.props.checkLogin()

    this.props.getNews(this.state.activePage)
    this.state.pageContent = this.state.searchResults.slice(itemsOnPage * (this.state.activePage - 1), itemsOnPage * this.state.activePage)
    // this.props.pagination(1, this.props.newsArr);
  }

  handleChangeTitle (event) {
    this.setState({title: event.target.value})
  }

  handlePageClick (event) {
    // let page = event.target.id
    this.state.activePage = event.target.id
    this.state.pageContent = this.state.searchResults.slice(itemsOnPage * (event.target.id - 1), itemsOnPage * event.target.id)
    this.props.pagination(this.state.activePage, this.state.searchResults)

  }

  handleChangeText (event) {
    this.setState({text: event.target.value})
  }

  handleChangeTags (event) {
    this.setState({tags: event.target.value.split(',')})
  }

  returnArrayForPagination (arr) {
    if (arr && arr.length) {
      // myNews2 = new Array(Math.ceil(myNews.length/3))
      return _.fill(Array(Math.ceil(arr.length / itemsOnPage)), 'z')
    }
    return []
  }

  searchByTag (event) {
    this.setState({searchTag: event.target.innerHTML})
    this.state.activePage = 1
    this.props.getNews(1)
  }

  searchUpdated (term) {
    this.setState({searchTerm: term})
    this.props.getNews(this.state.activePage)
  }

  handleSubmitForm (event) {
    event.preventDefault()
    let formData = new FormData()
    // let data
    formData.append('title', this.state.title)
    formData.append('text', this.state.text)
    formData.append('tags', this.state.tags)
    formData.append('image', this.fileInput.files[0])

    // axios.post('/posts/test',formData)
    this.props.getForm(formData);

    // return;
    //
    // for (let [key, value] of formData.entries()) {
    //   this.state.data[key] = value
    // }
    // console.log('data--', this.state.data)
    // let qqz = Object.assign({}, this.state)
    // this.props.getForm(this.state.data)
    this.props.getNews(this.state.activePage)
    this.props.pagination(this.state.activePage, this.state.searchResults)
    this.state.title = ''
    this.state.text = ''
    this.state.tags = []
  }

  render () {
    // let myNews = this.props.newsArr
    this.props.getUser()
    this.state.username = this.props.username
    let myNews = _.cloneDeep(this.props.newsArr)
    let myNews2 = []
    if (myNews) {
      this.state.searchResults = _.cloneDeep(this.props.newsArr)
    }
    this.state.searchResults = this.state.searchResults.filter(createFilter(this.state.searchTag, 'tags'))
    this.state.searchResults = this.state.searchResults.filter(createFilter(this.state.searchTerm, ['title', 'text']))
    this.state.pageContent = this.state.searchResults.slice(itemsOnPage * (this.state.activePage - 1), itemsOnPage * this.state.activePage)
    // this.props.getNews(this.state.activePage, this.state.searchResults)
    return (
      <div className='col-lg-8 col-lg-offset-2' onLoad={this.searchUpdated}>
        {this.props.isLogged === true && (
          <form className='' encType='multipart/form-data' onSubmit={this.handleSubmitForm}>
            <label>
              <h3 className='pull-left'>Title:</h3><input name='title' className='form-control' placeholder='Add headline' required
                                                          type='text' value={this.state.title}
                                                          onChange={this.handleChangeTitle}/>
              <br />
              <h3 className='pull-left'>Text:</h3><textarea name='text' className='form-control' type='text' placeholder='Add text'
                                                            required value={this.state.text}
                                                            onChange={this.handleChangeText}/>
              <br />
              <h3 className='pull-left'>Tags:</h3><input name='tags' className='form-control' type='text' value={this.state.tags}
                                                         placeholder='Add tags' onChange={this.handleChangeTags}/>
              <br />
              <h3 className='pull-left'>Put image:</h3><input  ref={(input) => { this.fileInput = input; }} type='file' name='post-image' />
            </label>
            <button className='btn' value='Submit' onSubmit={this.handleSubmitForm}>Post</button>
          </form>
        )}

        <SearchInput className={className + ' form-control'} onChange={this.searchUpdated}/>
        <div>
          {this.props.tagsArr.map((tags, item) => {
            return (
              <span className={ this.state.searchTag == tags ? 'btn-xs btn btn-info active' : 'btn-xs btn btn-info' }
                    onClick={this.searchByTag} key={item}>{tags}</span>
            )
          })
          }
        </div>
        {this.state.pageContent.map((news, item) => {
          return (
            <div className='clearfix' key={item}>
              <h2 className='text-left'>{news.title}</h2>
              <img src={news.dest} />
              <p className='text-left'>{news.text}</p>
              <div className='pull-left'>
                {news.tags.map((tag, num) => {
                  return (
                    <span className='btn btn-sm btn-info' key={num}>{tag}</span>
                  )
                })}
                <div>{news.username}</div>
              </div>
            </div>
          )
        })}
        <ul className='col-lg-4 col-lg-offset-4 list-inline'>
          {this.returnArrayForPagination(this.state.searchResults).map((item, i) => {
            i++
            return (
              <button
                className={ this.state.activePage == i ? 'list-unstyled btn btn-info active' : 'list-unstyled btn btn-info' }
                onClick={this.handlePageClick} key={i}
                id={i}>{i}
              </button>
            )
          })}
        </ul>
      </div>
    )
  }
}

export default NameForm
