import { connect } from 'react-redux'
import { actions } from '../modules/news'
import { checkLogin } from '../../Login/modules/counter'
import { getUser } from '../../Profile/modules/counter'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the news:   */

import NameForm from '../components/forms'

/*  Object of action creators (can also be function that returns object).
    Keys will be passed as props to presentational components. Here we are
    implementing our wrapper around addTitle; the component doesn't care   */

const mapDispatchToProps = {
  getUser,
  checkLogin,
  ...actions
}

const mapStateToProps = (state) => ({
  newsArr : state.something.newsArr,
  pageArr: state.something.pageArr,
  pageContent: state.something.pageContent,
  inputValue:state.something,
  currentPage: state.something.currentPage,
  tagsArr: state.something.tagsArr,
  isLogged: state.login.isLogged,
  username: state.profile.username
})

/*  Note: mapStateToProps is where you should use `reselect` to create selectors, ie:

    import { createSelector } from 'reselect'
    const news = (state) => state.news
    const tripleCount = createSelector(news, (count) => count * 3)
    const mapStateToProps = (state) => ({
      news: tripleCount(state)
    })

    Selectors can compute derived data, allowing Redux to store the minimal possible state.
    Selectors are efficient. A selector is not recomputed unless one of its arguments change.
    Selectors are composable. They can be used as input to other selectors.
    https://github.com/reactjs/reselect    */

export default connect(mapStateToProps, mapDispatchToProps)(NameForm)
