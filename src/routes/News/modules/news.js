// ------------------------------------
// Constants
// ------------------------------------
import axios from 'axios'
import _ from 'lodash'
export const GET_VALUE_TEXT = 'GET_VALUE_TEXT'
export const GET_NEWS = 'GET_NEWS'
export const GET_VALUE_FORM = 'GET_VALUE_FORM'
export const TEST_ACTION = 'TEST_ACTION'
export const PAGINATION = 'PAGINATION'

// ------------------------------------
// Actions
// ------------------------------------
// Object.assign({}, state, { newsArr:action.sendPost, pageContent: action.sendPost.slice(3 * (action.currentPage - 1), 3 * action.currentPage) })
// if (action.pageContent === 1) {
//
// }
// export function tetsFunct (value) {
//   return { type: TEST_ACTION }
// }

export function getNews (page) {
  return dispatch => {
    axios({
      url: '/posts',
      method: 'get'
    }).then((response) => {
      return dispatch({
        type: GET_NEWS,
        currentPage: page,
        sendPost: response.data
      })
      //   return dispatch => {
//     dispatch({
//       type    : POST_ADDING_TEXT,
//       payload : getState().counter
//     })
//   }

    })
  }
}
//
// export function getText (title) {
//   return {
//     type: GET_VALUE_TEXT,
//     payload: title
//   }
// }
export function getForm (value) {
  return dispatch => {
    axios.post('/posts/test', value).then((response) => {
      // axios.put('/posts', { newPost: value }).then((success) => {
      return dispatch({
        type: GET_VALUE_FORM,
        payload: value
      })
      // })

    }).catch(function (error) {
      console.log('Not able to upload image', error)
    })
  }
}
export function pagination (page, amount) {
  return {
    type: PAGINATION,
    payload: page,
    pageAmount: new Array(Math.ceil(amount.length / 3)),
    pageCounter: 1
  }
}
/*  This is a thunk, meaning it is a function that immediately
 returns a function for lazy evaluation. It is incredibly useful for
 creating async actions, especially when combined with redux-thunk! */

// export const doubleAsync = (data) => {
//   return dispatch => {
//     dispatch({
//       type    : POST_ADDING_TEXT,
//       payload : getState().counter
//     })
//   }
// }

export const actions = {
  getForm,
  getNews,
  pagination
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_VALUE_FORM]: (state, action) => {
    return Object.assign({}, state, {
      title: action.payload,
      text: action.payload,
      tags: action.payload
    })
  },
  [PAGINATION]: (state, action) => {
    let qqz = _.map(action.pageAmount, (item) => (
      item = action.pageCounter++
    ))
    return Object.assign({}, state, {
      currentPage: action.payload,
      pageArr: qqz
    })
  },
  [GET_NEWS]: (state, action) => {
    let tagsCloud = _.map(action.sendPost, (item) => (
      item = item.tags
    ))
    return Object.assign({}, state, {
      newsArr: action.sendPost,
      tagsArr: _.uniq(_.flatten(tagsCloud))
    })
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  newsArr: [],
  contentPage: [],
  title: '',
  text: '',
  tags: '',
  currentPage: 1,
  pageArr: [],
  tagsArr: []
}
export default function newsReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
