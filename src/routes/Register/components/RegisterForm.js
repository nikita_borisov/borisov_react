import React from 'react'

export class RegisterForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      surname: '',
      email: '',
      password: '',
      username: ''
    }
    this.handleChangeName = this.handleChangeName.bind(this)
    this.handleChangeUsername = this.handleChangeUsername.bind(this)
    this.handleChangeSurname = this.handleChangeSurname.bind(this)
    this.handleChangeEmail = this.handleChangeEmail.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.handleSubmitForm = this.handleSubmitForm.bind(this)
  }

  handleChangeName (event) {
    this.setState({name: event.target.value})
  }
  handleChangeUsername (event) {
    this.setState({username: event.target.value})
  }

  handleChangeSurname (event) {
    this.setState({surname: event.target.value})
  }

  handleChangeEmail (event) {
    this.setState({email: event.target.value})
  }

  handleChangePassword (event) {
    this.setState({password: event.target.value})
  }

  handleSubmitForm (event) {
    event.preventDefault()
    let formData = Object.assign({}, this.state)
    this.props.registerForm(formData)
    this.state.name = ''
    this.state.surname = ''
    this.state.email = ''
    this.state.password = ''
  }

  render () {
    return (
      <div>
        <form onSubmit={this.handleSubmitForm}>
          <label>
            Username*
            <input type='text' value={this.state.username} onChange={this.handleChangeUsername} className='form-control' />
          </label>
          <br />
          <label>
            Name*
            <input type='text' value={this.state.name} onChange={this.handleChangeName} className='form-control' />
          </label>
          <br />
          <label>
            Surname*
            <input type='text' value={this.state.surname} onChange={this.handleChangeSurname} className='form-control' />
          </label>
          <br />
          <label>
            Email*
            <input type='email' value={this.state.email} onChange={this.handleChangeEmail} className='form-control' />
          </label>
          <br />
          <label>
            Password*
            <input type='password' value={this.state.password} onChange={this.handleChangePassword}
                   className='form-control' />
          </label>
          <br />
          <button className='btn btn-lg'>Sign up</button>
        </form>
      </div>
    )
  }
}

export default RegisterForm
