import React from 'react'
import RegisterForm from './RegisterForm'

export const Register = props => {
  return (
    <div style={{ margin: '0 auto' }} >
      <h2>Sign up:</h2>
      <RegisterForm />
    </div>
  )
}

export default Register
