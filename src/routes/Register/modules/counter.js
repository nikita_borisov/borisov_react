// ------------------------------------
// Constants
// ------------------------------------
import axios from 'axios'
import { browserHistory } from 'react-router'
export const GET_FORM = 'GET_FORM'
export const COUNTER_DOUBLE_ASYNC = 'COUNTER_DOUBLE_ASYNC'

// ------------------------------------
// Actions
// ------------------------------------

/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk! */

export function registerForm (value) {
  return {
    type: GET_FORM,
    payload: value,
    sendPost: axios({
      url: '/auth/register',
      method: 'post',
      data: value
    }).then(function (response) {
      if (response.data) {
        browserHistory.push('/login')
      }
    }).catch(function (err) {
      console.log('!!!error ', err)
    })
  }
}

export const actions = {
  registerForm
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_FORM]    : (state, action) => {
    return Object.assign({}, state, {
      name: action.payload,
      surname: action.payload,
      email: action.payload,
      password: action.payload,
      username: action.payload
    })
  },
  [COUNTER_DOUBLE_ASYNC] : (state, action) => state * 2
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  name: '',
  surname: '',
  email: '',
  password: '',
  username: ''
}
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
