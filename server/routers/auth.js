const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const modelPost = require('../models/post')
const passport = require('passport')
const modelUser = require('../models/modelUser')
const LocalStrategy = require('passport-local').Strategy

router.post('/login', function (req, res, done) {
  passport.authenticate('local', (err, user, info) => {
    if (user) {
      // console.log(user)
      return res.status(200).send(user)
    } else {
      // console.log(user)
      return res.status(304).send('this is cool - no user ')
    }
  })(req, res, done)
})

router.get('/logout', function (req, res) {
  delete req.session.user
  // console.log('req.session--', req.session)
  req.logout()
  res.redirect('/something')
})

router.get('/facebook', passport.authenticate('facebook'))

router.get('/facebook/callback', passport.authenticate('facebook'),
  function (req, res) {
    // console.log('/facebook/callback--------------->', req.user)
    req.session.user = req.user
    // Successful authentication, redirect home.
    res.redirect('/')
  }
)

router.get('/gogogo', function (req, res, done) {
  passport.authenticate('facebook-local', (err, user, info) => {
    if (user) {
      // console.log('user--', user)
    } else {
      // console.log('err--', err, user, info)
    }
  })(req, res, done)
})

router.get('/profile', isLoggedIn, function (req, res) {
  res.status(200).send(req.session.user)
})

router.post('/register', function (req, res, done) {
  passport.authenticate('local-signup', (err, user, info) => {
    // console.log('err', err, 'user', user, 'info', info)
    if (user) {
      // console.log('Success login!!!')
      return res.status(200).send(req.body)
    }
    else if (err) {
      return res.status(201).send(info)
    }
  })(req, res, done)
})

function isLoggedIn (req, res, next) {
  // if user is authenticated in the session, carry on
  if (req.session.user) {
    return next()
  }
  else res.redirect('/login')
}

module.exports = router
