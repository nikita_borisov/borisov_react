const express = require('express')
const path = require('path')
const webpack = require('webpack')
const logger = require('../build/lib/logger')
const webpackConfig = require('../build/webpack.config')
const project = require('../project.config')
const compress = require('compression')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const posts = require('./posts')
const auth = require('./routers/auth')
const passport = require('passport')
const cookieParser = require('cookie-parser')
const session = require('express-session')
/*const LocalStrategy = require('passport-local')
const modelUser = require('./models/modelUser')*/

const app = express()
app.use(compress())

// ------------------------------------
// Apply Webpack HMR Middleware
// ------------------------------------

if (project.env === 'development') {
  const compiler = webpack(webpackConfig)
  logger.info('Enabling webpack development and HMR middleware')
  app.use(require('webpack-dev-middleware')(compiler, {
    publicPath: webpackConfig.output.publicPath,
    contentBase: path.resolve(project.basePath, project.srcDir),
    hot: true,
    quiet: false,
    noInfo: false,
    lazy: false,
    stats: 'normal',
  }))
  app.use(require('webpack-hot-middleware')(compiler, {
    path: '/__webpack_hmr'
  }))
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({extended: true}))
  app.use(express.static('public'));
  console.log('123123',__dirname)
  app.use(express.static(__dirname + '/images/post_images'))
  app.use(cookieParser())
  // app.use(session({
  //   secret: 'keyboard cat',
  //   proxy: true,
  //   resave: true,
  //   saveUninitialized: true
  // }))
  app.use(session({
    secret: 'mySecretKey',
    saveUninitialized: true,
    resave: true,
  }))
  app.use(passport.initialize());
  app.use(passport.session());



  require('./config/passport')(passport) // pass config for configuration

  app.use('/posts', posts)
  app.use('/auth', auth)

  // Serve static assets from ~/public since Webpack is unaware of
  // these files. This middleware doesn't need to be enabled outside
  // of development since this directory will be copied into ~/dist
  // when the application is compiled.
  app.use(express.static(path.resolve(project.basePath, 'public')))

  // This rewrites all routes requests to the root /index.html file
  // (ignoring file requests). If you want to implement universal
  // rendering, you'll want to remove this middleware.
  app.use('*', function (req, res, next) {
    const filename = path.join(compiler.outputPath, 'index.html')
    compiler.outputFileSystem.readFile(filename, (err, result) => {
      if (err) {
        return next(err)
      }
      res.set('content-type', 'text/html')
      res.send(result)
      res.end()
    })
  })
} else {
  logger.warn(
    'Server is being run outside of live development mode, meaning it will ' +
    'only serve the compiled application bundle in ~/dist. Generally you ' +
    'do not need an application server for this and can instead use a web ' +
    'server such as nginx to serve your static files. See the "deployment" ' +
    'section in the README for more information on deployment strategies.'
  )

  // Serving ~/dist by default. Ideally these files should be served by
  // the web server and not the app server, but this helps to demo the
  // server in production.
  app.use(express.static(path.resolve(project.basePath, project.outDir)))
}

let db = mongoose.connect('mongodb://localhost/db', {
  useMongoClient: true
})
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('Running DB')
})

module.exports = app
