const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const postSchema = new mongoose.Schema({
  title: String,
  text: String,
  tags: Array,
  name: String,
  surname: String,
  username: String,
  dest: String
})
const modelPost = mongoose.model('Posts', postSchema)

module.exports = modelPost
