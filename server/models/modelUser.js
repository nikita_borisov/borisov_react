const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new mongoose.Schema({
  name: String,
  surname: String,
  email: String,
  password: String,
  username: String,
  facebookId: String
})

const modelUser = mongoose.model('modelUser', userSchema)
module.exports = modelUser
