const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const modelPost = require('./models/post')
const modelUser = require('./models/modelUser')
const multer = require('multer')
console.log(123123)

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    console.log('--->', __dirname)
    callback(null, __dirname + '/images/post_images')

  },
  filename: function (req, file, callback) {
    // console.log('---->', file)
    callback(null, Date.now() + file.originalname)
  }
})
const upload = multer({storage: storage})
const fs = require('fs')

router.post('/test', upload.single('image'), function (req, res, next) {
  // console.log(req.file.destination + req.file.filename)
  // console.log('req.body--', req.body)
  let post = req.body
  if (req.file) {
    post.dest = '/' + req.file.filename
  }
  console.log('post--', post)
  modelPost.create(post, function (err, result) {
    if (err) {
      console.log('error')
      return res.status(500).send('Error!')
    }
    // console.log(result)
    // res.status(200).send(result)
  })
  res.sendStatus(200)
  // req.files is array of `photos` files
  // req.body will contain the text fields, if there were any
})

router.post('/', function (req, res, next) {
  let post = req.body.newPost
  modelPost.create(post, function (err, result) {
    if (err) {
      console.log('error')
      return res.status(500).send('Error!')
    }
    // console.log(result)
    res.status(200).send(result)
  })
})

router.post('/tags', function (req, res, next) {
  let post = req.body.newPost
  console.log(' req.body.newPost--', req.body.newPost)
  modelPost.create(post, function (err, result) {
    // post.tags = post.tags.split(',')
    if (err) {
      console.log('error')
      return res.status(500).send('Error!')
    }
    console.log(result)
    res.status(200).send(result)
  })
})

router.get('/', function (req, res) {
  modelPost.find(function (err, result) {
    if (err) {
      res.status(500).send('Error!')
    }
    res.status(200).send(result)
  })
})

// router.get('/users', function (req, res) {
//   modelUser.find(function (err, result) {
//     if (err) {
//       res.status(500).send('Error!')
//     }
//     res.status(200).send(result)
//   })
// })

router.get('/search', function (req, res) {
  modelPost.find(function (err, result) {
    if (err) {
      res.status(500).send('Error!')
    }
    res.status(200).send(result)
  })
})

router.delete('/', function (req, res) {
  modelPost.remove(function (err) {
    if (err) {
      res.status(500).send('Nothing was removed')
    }
    res.status(200).send('All items have been removed form DB')
  })
})

router.delete('/done', function (req, res) {
  modelPost.remove({checked: true}, function (err) {
    if (err) throw err
    console.log('Removed')
  })
})

router.delete('/:id', function (req, res) {
  let index = req.params.id
  modelPost.remove({_id: index}, function (err, result) {
    if (err) {
      return res.status(500).send('Nothing was removed')
    }
    res.status(200).send('Removed:  ', result)
  })
})

router.put('/check_all', function (req, res) {
  let query = {checked: false}
  let options = {multi: true}
  modelPost.update(query, {$set: {checked: true}}, options, function (err, num) {
    if (err) {
      return res.status(500).send('Error!')
    }
    res.status(200).send('checked', num)
  })
})

router.put('/uncheck_all', function (req, res) {
  let query = {checked: true}
  let options = {multi: true}
  modelPost.update(query, {$set: {checked: false}}, options, function (err, num) {
    if (err) {
      return res.status(500).send('Error!')
    }
    res.status(200).send('checked', num)
  })
})

router.put('/:id', function (req, res) {
  let query = {_id: req.params.id}
  modelPost.update(query, {$set: {checked: true}}, function (err, num) {
    if (err) {
      return res.status(500).send('Error!')
    }
    res.status(200).send('checked', num)
  })
})

router.put('/check/:id', function (req, res) {
  let query = {_id: req.params.id}
  modelPost.update(query, {$set: {checked: false}}, function (err, num) {
    if (err) {
      return res.status(500).send('Error!')
    }
    res.status(200).send('checked', num)
  })
})
router.put('/text/:id', function (req, res) {
  let query = {_id: req.params.id}
  let textEdited = req.body.text
  modelPost.update(query, {$set: {text: textEdited}}, function (err, num) {
    if (err) {
      return res.status(500).send('Error!')
    }
    res.status(200).send('Edited', num)
  })
})

module.exports = router
