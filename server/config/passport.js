const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const FacebookStrategy = require('passport-facebook').Strategy
const modelUser = require('../models/modelUser')

module.exports = function () {

  // =========================================================================
  // passport session setup ==================================================
  // =========================================================================
  // 478618979174197
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session

  // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user.id)
  })

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    modelUser.findById(id, function (err, user) {
      done(err, user)
    })
  })

  passport.use('local', new LocalStrategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function (req, email, password, done) { // callback with email and password from our form
      // find a user whose email is the same as the forms email
      // we are checking to see if the user trying to login already exists
      modelUser.findOne({email: email}, function (err, user) {
        // if there are any errors, return the error before anything else
        if (err) {
          return done(err)
        }
        // if no user is found, return the message
        if (!user) {
          return done(null, false, {error: 'loginMessage ,No user found.'}) // req.flash is the way to set flashdata using connect-flash
        }
        // if the user is found but the password is wrong
        // if (!user.validPassword(password)) {
        //   return done(null, false, {error: 'loginMessage ,Oops! Wrong password.'}) // create the loginMessage and save it to session as flashdata
        // }
        // all is well, return successful user
        req.session.user = user
        console.log('req.user--', req.session.user)
        return done(null, user)
      })
    }))

  passport.use('facebook-local', new LocalStrategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField: 'facebookId',
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function (req, facebookId, done) { // callback with email and password from our form
      // find a user whose email is the same as the forms email
      // we are checking to see if the user trying to login already exists
      console.log('facebookId--', facebookId)
      modelUser.findOne({facebookId: facebookId}, function (err, user) {

        // if there are any errors, return the error before anything else
        if (err) {
          return done(err)
        }
        // if no user is found, return the message
        if (!user) {
          return done(null, false, {error: 'loginMessage ,No user found.'}) // req.flash is the way to set flashdata using connect-flash
        }
        // if the user is found but the password is wrong
        // if (!user.validPassword(password)) {
        //   return done(null, false, {error: 'loginMessage ,Oops! Wrong password.'}) // create the loginMessage and save it to session as flashdata
        // }
        // all is well, return successful user
        req.session.user = user
        return done(null, user)
      })
    }))

  passport.use('facebook', new FacebookStrategy({
      clientID: '478618979174197',
      clientSecret: '68ab44c21190cbde98338ee56db9f115',
      callbackURL: 'http://71a1275b.ngrok.io/auth/facebook/callback'
    },
    function (accessToken, refreshToken, profile, done) {
      modelUser.findOne({facebookId: profile.id}, function (err, user) {
        if (user === null) {
          let name = profile.displayName.split(' ')
          console.log('name--', name)
          modelUser.create({
            name: name[0],
            surname: name[1],
            facebookId: profile.id
          }, function (req, res) {
            return done(null, user)
          })
        }
        else {
          done(null, user)
        }

      })
    }
  ))

  passport.use('local-signup', new LocalStrategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function (req, email, password, done) { // callback with email and password from our form
      // find a user whose email is the same as the forms email
      // we are checking to see if the user trying to login already exists
      modelUser.findOne({email: email}, function (err, user) {
        if (user === null) {
          modelUser.create(req.body, function (err, user) {
            // if there are any errors, return the error before anything else
            if (err) {
              return done(err)
            }
            return done(null, user)
          })
        }
        else {
          let err = {message: 'Email is already used'}
          let info = {message: 'Please choose another Email'}
          return done(err, null, info)
        }
      })
    }))

}
